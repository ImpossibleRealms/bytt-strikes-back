package com.gravitygamesinteractive.byttstrikesback;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.BufferedInputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.gravitygamesinteractive.byttstrikesback.text.Text;
import java.awt.event.KeyEvent;

public class Menu extends JComponent{
	
	public static int option=1;
	public static int currentMenu=1;
	public static int screenSize = 2;
	public static boolean blip=false,confirmed=false;;
	public static ImageIcon pointer;
	public static ImageIcon a,l,p,y;
	public static ImageIcon Stage1,Stage2,Stage3;
        public KeyListen keylist;
        
        public int menuScrollY=0;
	
	public Menu(KeyListen keylist){
            this.keylist = keylist;
            pointer=new ImageIcon("res/resources/images/Font/Pointer.png");
            a=new ImageIcon("res/resources/images/Font/A.png");
            l=new ImageIcon("res/resources/images/Font/L.png");
            p=new ImageIcon("res/resources/images/Font/P.png");
            y=new ImageIcon("res/resources/images/Font/Y.png");
            Stage1=new ImageIcon("res/resources/images/LevelIcons/1.png");
            Stage2=new ImageIcon("res/resources/images/LevelIcons/2.png");
            Stage3=new ImageIcon("res/resources/images/LevelIcons/3.png");
	}

	public void tick(){
		if(blip){
			playSound();
		}
		if(confirmed){
			playSoundConfirm();
		}
	}
	
	public void paintComponent(Graphics2D g){
		if(currentMenu==1){
			if (option==1){
				pointer.paintIcon(this,g,Frame.gameSize.width/2-60, 150);
			}else if (option==2){
				pointer.paintIcon(this,g,Frame.gameSize.width/2-60, 165);
			}else if (option==3){
				pointer.paintIcon(this,g,Frame.gameSize.width/2-60, 180);
			}else if (option==4){
				pointer.paintIcon(this,g,Frame.gameSize.width/2-60, 195);
			}else if (option==5){
				pointer.paintIcon(this,g,Frame.gameSize.width/2-60, 210);
			}
		
		//Text text = new Text("PQRSTUVWXYZ[\\]^_");
		//text.render(g, Main.font, Frame.gameSize.width/2, 150);
		g.setColor(Color.black);
		//g.drawString("Start", Frame.gameSize.width/2-20, 170);
		Text.drawString("START", g, Main.font, Frame.gameSize.width/2, 155, Text.CENTER);
		Text.drawString("OPTIONS", g, Main.font, Frame.gameSize.width/2, 170, Text.CENTER);
		//g.drawString("Options", Frame.gameSize.width/2-20, 185);
		Text.drawString("CREDITS", g, Main.font, Frame.gameSize.width/2, 185, Text.CENTER);
		//g.drawString("Mods", Frame.gameSize.width/2-20, 200);
		Text.drawString("MODS", g, Main.font, Frame.gameSize.width/2, 200, Text.CENTER);
		//g.drawString("Quit", Frame.gameSize.width/2-20, 215);
		Text.drawString("QUIT", g, Main.font, Frame.gameSize.width/2, 215, Text.CENTER);	
		/*p.paintIcon(this,g,Frame.size.width/2-20,288);
		l.paintIcon(this,g,Frame.size.width/2-4,288);
		a.paintIcon(this,g,Frame.size.width/2+10,288);
		y.paintIcon(this,g,Frame.size.width/2+26,288);*/
		/*g.setColor(Color.GRAY);
		g.drawString("How to Play", Frame.size.width/2-20, 320);
		g.drawString("Options", Frame.size.width/2-20, 340);
		g.drawString("Level Editor", Frame.size.width/2-20, 360);*/
	}else if(currentMenu==2){
		g.setColor(Color.black);
		//g.drawString("Choose a Stage!", Frame.gameSize.width/2-40, 20);
		Text.drawString("CHOOSE A STAGE!", g, Main.font, Frame.gameSize.width/2, 0, Text.CENTER);
		Stage1.paintIcon(this,g,50,100);
		Stage2.paintIcon(this,g,150,100);
		Stage3.paintIcon(this,g,250,100);
		if (option==1){
			pointer.paintIcon(this,g,30, 115);
		}
		if (option==2){
			pointer.paintIcon(this,g,130, 115);
		}
		if (option==3){
			pointer.paintIcon(this,g,230, 115);
		}
	}else if(currentMenu==3){
		Text.drawString("CONFIGURE CONTROLS", g, Main.font, Frame.gameSize.width/2, 30, Text.CENTER);
		Text.drawString("VIDEO OPTIONS", g, Main.font, Frame.gameSize.width/2, 45, Text.CENTER);
		Text.drawString("AUDIO OPTIONS", g, Main.font, Frame.gameSize.width/2, 60, Text.CENTER);
		Text.drawString("GAME OPTIONS", g, Main.font, Frame.gameSize.width/2, 75, Text.CENTER);
		if (option==1){
			pointer.paintIcon(this,g,60, 30-5);
		}
		if (option==2){
			pointer.paintIcon(this,g,60, 45-5);
		}
		if (option==3){
			pointer.paintIcon(this,g,60, 60-5);
		}
		if (option==4){
			pointer.paintIcon(this,g,60, 75-5);
		}
		//Text.drawString("CONFIGURE CONTROLS", g, Main.font, Frame.gameSize.width/2, 90, Text.CENTER);
	}else if(currentMenu==4){
		Text.drawString("FONT BY CLINT BELLANGER", g, Main.font, Frame.gameSize.width/2, 30, Text.CENTER);
		
		Text.drawString("EVERYTHING ELSE BY", g, Main.font, Frame.gameSize.width/2, 60, Text.CENTER);
		Text.drawString("IMPOSSIBLE REALMS/", g, Main.font, Frame.gameSize.width/2, 75, Text.CENTER);
		Text.drawString("GRAVITY GAMES INTERACTIVE", g, Main.font, Frame.gameSize.width/2, 90, Text.CENTER);
	}else if(currentMenu==5){
		
	}else if(currentMenu==6){
            
            menuScrollY=0;
            
            if(option>7){
                menuScrollY = (15*(option-7));
                if(menuScrollY>60){
                    menuScrollY=60;
                }
            }
            
            Color temp = g.getColor();
            g.setColor(new Color(255, 0, 0, 128));
            
            int tempY = (15*option)+27;
            if(option >= 9){
                tempY += 25;
            }
            g.fillRect(15, tempY-menuScrollY, 290, 14);
            
                Text.drawString("CONTROLS SETUP", g, Main.font, Frame.gameSize.width/2, 10-menuScrollY, Text.CENTER);
		Text.drawString("GAME CONTROLS", g, Main.font, Frame.gameSize.width/2, 25-menuScrollY, Text.CENTER);
		Text.drawString(keylist.keymapNames[0], g, Main.font, 20, 45-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[1], g, Main.font, 20, 60-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[2], g, Main.font, 20, 75-menuScrollY, Text.LEFT);
                Text.drawString(keylist.keymapNames[3], g, Main.font, 20, 90-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[4], g, Main.font, 20, 105-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[5], g, Main.font, 20, 120-menuScrollY, Text.LEFT);
                Text.drawString(keylist.keymapNames[6], g, Main.font, 20, 135-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[7], g, Main.font, 20, 150-menuScrollY, Text.LEFT);
		Text.drawString("MENU CONTROLS", g, Main.font, Frame.gameSize.width/2, 170-menuScrollY, Text.CENTER);
                Text.drawString(keylist.keymapNames[8], g, Main.font, 20, 190-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[9], g, Main.font, 20, 205-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[10], g, Main.font, 20, 220-menuScrollY, Text.LEFT);
                Text.drawString(keylist.keymapNames[11], g, Main.font, 20, 235-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[12], g, Main.font, 20, 250-menuScrollY, Text.LEFT);
		Text.drawString(keylist.keymapNames[13], g, Main.font, 20, 265-menuScrollY, Text.LEFT);
                //Display the current set keys. WARNING: Font needs to support lowercase letters now.
                Text.drawString(KeyEvent.getKeyText(keylist.keymap[0]).toUpperCase(), g, Main.font, 300, 45-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[1]).toUpperCase(), g, Main.font, 300, 60-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[2]).toUpperCase(), g, Main.font, 300, 75-menuScrollY, Text.RIGHT);
                Text.drawString(KeyEvent.getKeyText(keylist.keymap[3]).toUpperCase(), g, Main.font, 300, 90-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[4]).toUpperCase(), g, Main.font, 300, 105-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[5]).toUpperCase(), g, Main.font, 300, 120-menuScrollY, Text.RIGHT);
                Text.drawString(KeyEvent.getKeyText(keylist.keymap[6]).toUpperCase(), g, Main.font, 300, 135-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[7]).toUpperCase(), g, Main.font, 300, 150-menuScrollY, Text.RIGHT);
                
                Text.drawString(KeyEvent.getKeyText(keylist.keymap[8]).toUpperCase(), g, Main.font, 300, 190-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[9]).toUpperCase(), g, Main.font, 300, 205-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[10]).toUpperCase(), g, Main.font, 300, 220-menuScrollY, Text.RIGHT);
                Text.drawString(KeyEvent.getKeyText(keylist.keymap[11]).toUpperCase(), g, Main.font, 300, 235-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[12]).toUpperCase(), g, Main.font, 300, 250-menuScrollY, Text.RIGHT);
		Text.drawString(KeyEvent.getKeyText(keylist.keymap[13]).toUpperCase(), g, Main.font, 300, 265-menuScrollY, Text.RIGHT);
                
                if(keylist.remappingKey){
                    g.setColor(new Color(255, 0, 0, 200));
                    g.fillRect(15, 45, 290, 140);
                    Text.drawString("PRESS A NEW KEY FOR", g, Main.font, Frame.gameSize.width/2, 60, Text.CENTER);
                    Text.drawString("MOVE LEFT", g, Main.font, Frame.gameSize.width/2, 75, Text.CENTER);
                }
                
                g.setColor(temp);
	}else if(currentMenu==7){
		//Text.drawString("SCREEN SIZE", g, Main.font, Frame.gameSize.width/2, 30, Text.CENTER);
		Text.drawString("SCREEN SIZE   < " + Integer.toString((Frame.windowSize.width/Frame.gameSize.width)) + "X >", g, Main.font, Frame.gameSize.width/2, 30, Text.CENTER);
	}
	}
	private void playSound(){
		try{
			BufferedInputStream spawn = new BufferedInputStream(getClass().getResourceAsStream("MenuBeep.wav")); 
	        Clip clip = AudioSystem.getClip();
	        // getAudioInputStream() also accepts a File or InputStream
	        AudioInputStream spawnsound = AudioSystem.getAudioInputStream(spawn);
	        clip.open(spawnsound);
	        clip.loop(0);
	        /*if(jumpedsound=true){
	        clip.close();
	        }*/
	        blip=false;
		}catch(Exception e){
			 e.printStackTrace();
		}
	}
	private void playSoundConfirm(){
		try{
			BufferedInputStream spawn = new BufferedInputStream(getClass().getResourceAsStream("Confirm.wav")); 
	        Clip clip = AudioSystem.getClip();
	        // getAudioInputStream() also accepts a File or InputStream
	        AudioInputStream spawnsound = AudioSystem.getAudioInputStream(spawn);
	        clip.open(spawnsound);
	        clip.loop(0);
	        /*if(jumpedsound=true){
	        clip.close();
	        }*/
	        confirmed=false;
		}catch(Exception e){
			 e.printStackTrace();
		}
	}
	
}
