package com.gravitygamesinteractive.byttstrikesback;

import java.awt.Dimension;
import java.awt.event.*;

public class KeyListen implements KeyListener{
	public static boolean downpressed=false;
        
        public static boolean remappingKey=false;
        
        /*public int menuLeftKey = KeyEvent.VK_LEFT;
        public int menuRightKey = KeyEvent.VK_RIGHT;
        public int menuUpKey = KeyEvent.VK_UP;
        public int menuDownKey = KeyEvent.VK_DOWN;
        public int confirmKey = KeyEvent.VK_Z;
        public int backKey = KeyEvent.VK_X;
        
        public int leftKey = KeyEvent.VK_LEFT;
        public int rightKey = KeyEvent.VK_RIGHT;
        public int upKey = KeyEvent.VK_UP;
        public int downKey = KeyEvent.VK_DOWN;
        public int cycleLeft = KeyEvent.VK_Z;
        public int cycleRight = KeyEvent.VK_X;
        public int spawnMinion = KeyEvent.VK_SPACE;
        public int ready = KeyEvent.VK_ENTER;*/
        
        public int[]keymap;
        public String[]keymapNames = {"MOVE LEFT", "MOVE RIGHT", "MOVE UP", 
            "MOVE DOWN", "CYCLE LEFT", "CYCLE RIGHT", "SPAWN MINION", "READY", 
            "MENU LEFT", "MENU RIGHT", "MENU UP", "MENU DOWN", 
            "CONFIRM", "BACK"};
        //public int
        public KeyListen(){
            keymap = new int[14];
            keymap[0] = KeyEvent.VK_LEFT;
        keymap[1] = KeyEvent.VK_RIGHT;
        keymap[2] = KeyEvent.VK_UP;
        keymap[3] = KeyEvent.VK_DOWN;
        keymap[4] = KeyEvent.VK_Z;
        keymap[5] = KeyEvent.VK_X;
        keymap[6] = KeyEvent.VK_SPACE;
        keymap[7] = KeyEvent.VK_ENTER;
        
        keymap[8] = KeyEvent.VK_LEFT;
        keymap[9] = KeyEvent.VK_RIGHT;
       keymap[10] = KeyEvent.VK_UP;
        keymap[11] = KeyEvent.VK_DOWN;
        keymap[12] = KeyEvent.VK_Z;
        keymap[13] = KeyEvent.VK_X;
        
        }

	public void keyPressed(KeyEvent e){
		int key = e.getKeyCode();
                
                if(Menu.currentMenu==6 && remappingKey){
                    remappingKey=false;
                    keymap[Menu.option-1] = key;
                }else{
		
		if(!Main.menuon && !Level.levelOver){
                    if(key == keymap[1]){
                        Frame.isMoving = true;
                        Frame.dir = Character.moveSpeed;
                        Main.Kylex = Level.kyle.x - 8 - Frame.sx;
                        Main.Kyley = Level.kyle.y - Frame.sy;
                    }
                    if(key == keymap[0]){
                        Frame.isMoving = true;
			Frame.dir = -Character.moveSpeed;
                    }
                    if(key == keymap[2]){
                        Character.movingup = true;
			Character.movingdown = false;
                    }
                    if(key == keymap[3]){
                        Character.movingup = false;
			Character.movingdown = true;
                    }
                    if(key == keymap[4]){
                        if(Level.currentEnemy == 1){
                            Level.currentEnemy = 2;
			}else{
                            Level.currentEnemy -= 1;
			}
                    }
                    if(key == keymap[5]){
                        if(Level.currentEnemy == 2){
                            Level.currentEnemy = 1;
			}else{
                            Level.currentEnemy += 1;
			}
                    }
                    if(key == keymap[6]){
                        Character.spawningEnemy = true;
			Character.spaceDown = true;
                    }
                    if(key == keymap[7]){
                        if(Level.Timer > 0){
                            Level.Timer = 0;
			}else{
                            Frame.sx = 0;
			}
                    }
		}else if(Main.menuon){
                    
                    if(key == keymap[9]){
                        if(Menu.currentMenu == 2){
                            if(Menu.option == 3){
				Menu.option = 1;
                            }else{
				Menu.option += 1;
                            }
                            Menu.blip = true;
			}else if(Menu.currentMenu == 7){
                            if(Menu.option == 1){
				if(Menu.screenSize < Frame.screenSize.height / Frame.gameSize.height){
                                    Menu.screenSize++;
                                    Frame.windowSize = new Dimension(Frame.gameSize.width * Menu.screenSize, Frame.gameSize.height * Menu.screenSize);
                                    Frame.frame.setSize(Frame.windowSize);
                                    Frame.frame.setLocationRelativeTo(null);
				}else{
                                    Menu.screenSize = 1;
                                    Frame.windowSize = new Dimension(Frame.gameSize.width * Menu.screenSize, Frame.gameSize.height * Menu.screenSize);
                                    Frame.frame.setSize(Frame.windowSize);
                                    Frame.frame.setLocationRelativeTo(null);
				}
                            }else{
							
                            }
                            Menu.blip = true;
			}
                    }
                    if(key == keymap[8]){
                        if(Menu.currentMenu == 2){
                            if(Menu.option == 1){
				Menu.option = 3;
                            }else{
				Menu.option -= 1;
                            }
                            Menu.blip = true;
			}else if(Menu.currentMenu == 7){
                            if(Menu.option == 1){
				if(Menu.screenSize > 1){
                                    Menu.screenSize--;
                                    Frame.windowSize = new Dimension(Frame.gameSize.width*Menu.screenSize, Frame.gameSize.height*Menu.screenSize);
                                    Frame.frame.setSize(Frame.windowSize);
                                    Frame.frame.setLocationRelativeTo(null);
				}else{
                                    Menu.screenSize = Frame.screenSize.height/Frame.gameSize.height;
                                    Frame.windowSize = new Dimension(Frame.gameSize.width * Menu.screenSize, Frame.gameSize.height * Menu.screenSize);
                                    Frame.frame.setSize(Frame.windowSize);
                                    Frame.frame.setLocationRelativeTo(null);
				}
                            }else{
						
                            }
                            Menu.blip = true;
			}
                    }
                    if(key == keymap[10]){
                        if(Menu.currentMenu == 1){
                            if(Menu.option == 1){
				Menu.option = 5;
                            }else{
				Menu.option--;
                            }
                            Menu.blip=true;
			}else if(Menu.currentMenu == 3){
                            if(Menu.option == 1){
				Menu.option = 4;
                            }else{
				Menu.option--;
                            }
                            Menu.blip = true;
			}else if(Menu.currentMenu == 6 && !remappingKey){
                            if(Menu.option == 1){
				Menu.option = 14;
                            }else{
				Menu.option--;
                            }
                            Menu.blip = true;
			}
                    }
                    if(key == keymap[11]){
                        if(Menu.currentMenu == 1){
                            if(Menu.option == 5){
				Menu.option = 1;
                            }else{
				Menu.option++;
                            }
				Menu.blip=true;
			    }else if(Menu.currentMenu == 3){
					if(Menu.option == 4){
						Menu.option = 1;
					}else{
						Menu.option++;
					}
				Menu.blip=true;
			    }else if(Menu.currentMenu == 6 && !remappingKey){
                            if(Menu.option == 14){
				Menu.option = 1;
                            }else{
				Menu.option++;
                            }
				Menu.blip=true;
			    }
                    }
                    if(key == keymap[12]){
                        Menu.confirmed=true;
			if(Menu.currentMenu==1){
                            if(Menu.option==1){
				Menu.currentMenu=2;
				Menu.option=1;
                            }else if(Menu.option==2){
				Menu.currentMenu=3;
				Menu.option=1;
                            }else if(Menu.option==3){
				Menu.currentMenu=4;
				Menu.option=1;
                            }else if(Menu.option==4){
				//TODO: Mods Menu and Support
                            }else if(Menu.option==5){
				System.exit(0);
                            }
			}else if(Menu.currentMenu==2){
                            if(Menu.option==1){
				Main.levelname=new String("assets/VerdantValley1");
                            }
                            if(Menu.option==2){
				Main.levelname=new String("assets/VerdantValley2");
                            }
                            if(Menu.option==3){
				Main.levelname=new String("assets/VerdantValley4");
                            }
                            Main.menuon=false;
                            Level.levelOver=false;
                            Level.levelWon=false;
			}else if(Menu.currentMenu==3){
                            if(Menu.option==1){
                                Menu.currentMenu=6;
                            }else if(Menu.option==2){
				Menu.currentMenu=7;
                            }else if(Menu.option==3){
				//Menu.currentMenu=4;
                            }
                            Menu.option=1;
			}else if(Menu.currentMenu==4){
                            Menu.currentMenu=1;
                            Menu.option = 1;
			}else if(Menu.currentMenu==6 && !remappingKey){
                            remappingKey=true;
                        }
                    }
                    if(key == keymap[13]){
                        Menu.confirmed=true;
	        	if(Menu.currentMenu==1){
					
			}else if(Menu.currentMenu==2){
                            Menu.currentMenu=1;
                            Menu.option=1;
			}else if(Menu.currentMenu==3){
                            Menu.currentMenu=1;
                            Menu.option=1;
			}else if(Menu.currentMenu==6 && !remappingKey){
                            Menu.currentMenu=3;
                            Menu.option=1;
			}else if(Menu.currentMenu==7){
                            Menu.currentMenu=3;
                            Menu.option=1;
			}
                    }
                    
		}else if(Level.levelOver){
                    /*if(key == menuLeftKey || key == menuRightKey || key == menuUpKey || key == menuDownKey || key == confirmKey || key == backKey || key == leftKey
                            || key == rightKey || key == upKey || key == downKey || key == cycleLeft || key == cycleRight || key == spawnMinion || key == ready){*/
                    for(int i=0; i<keymap.length; i++){
                        if(key == keymap[i]){
                            Main.statscreenover=true;
                            Level.levelOver=false;
                            Level.levelWon=false;
                            break;
                        }
                    }
		}
                }
	}

	public void keyReleased(KeyEvent e){
		int key = e.getKeyCode();
		
                if(key == keymap[1]){
                    if(Frame.dir==Character.moveSpeed){
			Frame.isMoving=true;
			Frame.dir=0;
                    }
                }
                if(key == keymap[0]){
                    if(Frame.dir==-Character.moveSpeed){
			Frame.isMoving=true;
			Frame.dir=0;
                    }
                }
                if(key == keymap[2]){
                    Character.movingup=false;
                    Character.movingdown=false;
                }
                if(key == keymap[3]){
                    Character.movingup=false;
                    Character.movingdown=false;
                }
                if(key == keymap[4]){
                    //Cycle Left, not sure if this is needed code
                    Character.allowFall = true;
                    downpressed = false;
                    if(Character.allowFall){
			Character.jumpCount = Character.jumpHeight;
                    }
                }
                if(key == keymap[6]){
                    Character.spaceDown = false;
                }
                
	}
	public void keyTyped(KeyEvent e){
		int key = e.getKeyCode();
		
		switch(key){
		case KeyEvent.VK_RIGHT:
		
		break;
		
		case KeyEvent.VK_LEFT:
			
			break;
		}
	}
}
